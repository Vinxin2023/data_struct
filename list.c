#include <stdio.h>
#include <stdlib.h>

typedef struct list_node
{
    struct list_node *next;
    int buf_data;
} LIST_NODE;

LIST_NODE *head = NULL; //创建头节点指针--方便操作链表
LIST_NODE *tail = NULL; //创建尾节点指针--方便操作链表

/**
* 功能：向“链表”中追加节点
* 添加方式：向尾部添加
**/
int Append_ListNode(int _data)
{
    LIST_NODE *p = (LIST_NODE *)malloc(sizeof(LIST_NODE));
    if (p == NULL)
    {
        return -1; //申请内存空间出错
    }
    //添加节点数据
    p->buf_data = _data;
    p->next = NULL;

    if (head == NULL) //链表为空
    {
        //头节点与尾节点指针都指向一起
        head = p;
        tail = p;
        tail->next = NULL;
        head->next = NULL;
    }
    else //链表中有数据
    {
        tail->next = p;
        tail = p;
        tail->next = NULL;
        p->next = NULL;
    }
    return 0;
}

/**
 * 功能：删除“链表”中数据
 * 删除方式：尾部删除
 **/
int Remove_LastNode(void)
{
    LIST_NODE *p = NULL;
    LIST_NODE *P_temp = NULL;
    p = head;

    if (p->next == NULL) //只有一个节点
    {
        return -1; //直接返回
    }
    while (p->next->next) // 遍历链表找到“倒数第二个”节点地址
    {
        p = p->next; //指向下一个节点地址
    }
    //此时p就是“倒数第二个”节点
    p->next = NULL;    //与后一节点“断链”
    P_temp = tail;     //获取“尾部”节点地址
    tail = p;          //将“倒数第二个”节点更改为“尾部”节点
    free(P_temp);      //释放"尾部"节点地址指向的内存--free函数只是将需要释放的内存区域做了标记并不会马上释放
    P_temp = NULL;     //不再使用P_temp指针,所以将它指针空
    p->next = NULL;    //尾部节点地址标准操作
    tail->next = NULL; //尾部节点地址标准操作
    return 0;          //移除成功
}

/**
 * 功能：查询链表中的数据
 * 返回：查到的数据对应的链表地址;NULL：没有找到
 **/
LIST_NODE *Search_dataFromList(int _data)
{
    LIST_NODE *p = NULL;
    p = head;
    while (p)
    {
        if (p->buf_data == _data)
        {
            return p;
        }
        p = p->next;
    }
    return NULL;
}

/**
 * 功能：插入节点到指定的数据位置，
 *      如果链表中存在此位置则在此位置后插入，
 *      否则插入到链表尾部
 * 返回：0: 成功; -1: 失败
 **/
int Insert_Node(int _data, int index)
{
    LIST_NODE *temp_p = NULL;
    LIST_NODE *p = NULL;
    int res = 0;

    p = (LIST_NODE *)malloc(sizeof(LIST_NODE)); //申请内存
    if (p == NULL)
    {
        return -1; //申请失败
    }
    p->buf_data = _data;
    p->next = NULL;

    temp_p = Search_dataFromList(index); //查询数据位置
    if (temp_p == NULL)                  //没有找到
    {
        res = Append_ListNode(_data); //插入到链表尾部
        if (res == -1)                // 插入失败
        {
            return -1; //失败
        }
    }
    else
    {
        //插入到给定的数据位置后面
        p->next = temp_p->next; //新节点与要插入点的节点的后一个节点连接
        temp_p->next = p;       //断链再与新节点接合
    }
    return 0;
}

/**
 * 功能：删除指定的元素
 **/
void Remove_Node(int _data)
{
    int i;
    LIST_NODE *p = NULL;
    LIST_NODE *after_p = NULL;
    p = head;

    //如果只有一个节点
    if (p->next == NULL)
    {
        return; //直接返回
    }

    //先查询链表中是否有给定的元素
    while (p)
    {
        //比较下一个节点的数据
        if (p->next->buf_data == _data)
        {
            break;
        }
        p = p->next;
    }
    //此时p的地址为查找到的数据的上一个节点的地址
    after_p = p->next;
    p->next = NULL; //先断链
    p->next = after_p->next;
    after_p->next = NULL;
    free(after_p);
    after_p = NULL;
}

/**
 * 功能：打印链表
 **/
void Print_List(LIST_NODE *p_head)
{
    LIST_NODE *temp = NULL;
    int len = 0; //链表长度
    temp = p_head;

    while (temp)
    {
        printf("%d,", temp->buf_data);
        temp = temp->next;
        len++;
    }
    printf("\n");
    printf("the length of list is %d\n\n", len);
}

int main(int argc, char *argv[])
{
    int i;
    printf("Create List!\n");
    for (i = 0; i < 10; i++) //创建链表
    {
        Append_ListNode(i);
    }
    Print_List(head);

    //删除链表中最后一个元素
    printf("Remove LastNode!\n");
    Remove_LastNode();
    Print_List(head);

    //从链表中删除指定元素
    printf("Remove Node!\n");
    Remove_Node(3);
    Print_List(head);

    //向链表中插入元素
    printf("Insert Node!\n");
    Insert_Node(3, 3);
    Print_List(head);

    return 0;
}
